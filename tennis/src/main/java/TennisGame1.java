public class TennisGame1 implements TennisGame {

    private int firstPlayerScore;
    private int secondPlayerScore;
    private final String firstPlayerName;
    private final String secondPlayerName;

    public TennisGame1(String firstPlayerName, String secondPlayerName) {
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(firstPlayerName)) {
            firstPlayerScore += 1;
        } else {
            secondPlayerScore += 1;
        }
    }

    public String getScore() {
        StringBuilder scoreStringDescription = new StringBuilder();
        if (firstPlayerScore == secondPlayerScore) {
            scoreStringDescription = validateScoreWhenScoreIsSame();
        } else if (firstPlayerScore >= 4 || secondPlayerScore >= 4) {
            scoreStringDescription = validateScoreWhenAdvOrWins();
        } else {
            validateScore(scoreStringDescription);
        }
        return scoreStringDescription.toString();
    }

    private void validateScore(StringBuilder scoreStringDescription) {
        int tempScore;
        for (int i = 1; i < 3; i++) {
            tempScore = getTempScore(scoreStringDescription, i);
            appendTempScore(scoreStringDescription, tempScore);
        }
    }

    private void appendTempScore(StringBuilder scoreStringDescription, int tempScore) {
        switch (tempScore) {
            case 0:
                scoreStringDescription.append("Love");
                break;
            case 1:
                scoreStringDescription.append("Fifteen");
                break;
            case 2:
                scoreStringDescription.append("Thirty");
                break;
            case 3:
                scoreStringDescription.append("Forty");
                break;
        }
    }

    private int getTempScore(StringBuilder scoreStringDescription, int i) {
        int tempScore;
        if (i == 1) tempScore = firstPlayerScore;
        else {
            scoreStringDescription.append("-");
            tempScore = secondPlayerScore;
        }
        return tempScore;
    }

    private StringBuilder validateScoreWhenAdvOrWins() {
        StringBuilder scoreStringDescription;
        int minusResult = firstPlayerScore - secondPlayerScore;
        if (minusResult == 1) scoreStringDescription = new StringBuilder("Advantage player1");
        else if (minusResult == -1) scoreStringDescription = new StringBuilder("Advantage player2");
        else if (minusResult >= 2) scoreStringDescription = new StringBuilder("Win for player1");
        else scoreStringDescription = new StringBuilder("Win for player2");
        return scoreStringDescription;
    }

    private StringBuilder validateScoreWhenScoreIsSame() {
        StringBuilder score;
        switch (firstPlayerScore) {
            case 0:
                score = new StringBuilder("Love-All");
                break;
            case 1:
                score = new StringBuilder("Fifteen-All");
                break;
            case 2:
                score = new StringBuilder("Thirty-All");
                break;
            default:
                score = new StringBuilder("Deuce");
                break;

        }
        return score;
    }
}
