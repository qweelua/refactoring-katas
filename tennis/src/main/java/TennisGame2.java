
public class TennisGame2 implements TennisGame {

    private int firstPlayerPoints = 0;
    private int secondPlayerPoints = 0;
    private final String firstPlayerName;
    private final String secondPlayerName;

    public TennisGame2(String firstPlayerName, String secondPlayerName) {
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
    }

    public String getScore() {
        String score = "";
        if (isScoreIsTheSame()) {
            score = validatePlayerResult(firstPlayerPoints);
            score += "-All";
        }
        if (isScoreDeuce())
            score = "Deuce";

        String firstPlayerResult;
        String secondPlayerResult;
        if (isOnePlayerIsHaveZeroPoints(firstPlayerPoints, secondPlayerPoints)) {
            firstPlayerResult = validatePlayerResult(firstPlayerPoints);
            secondPlayerResult = "Love";
            score = firstPlayerResult + "-" + secondPlayerResult;
        }
        if (isOnePlayerIsHaveZeroPoints(secondPlayerPoints, firstPlayerPoints)) {
            secondPlayerResult = validatePlayerResult(secondPlayerPoints);
            firstPlayerResult = "Love";
            score = firstPlayerResult + "-" + secondPlayerResult;
        }

        if (isOnePlayerWinsAndHaveLessThanFourPoints(firstPlayerPoints, secondPlayerPoints)) {
            firstPlayerResult = validatePlayerResult(firstPlayerPoints);
            secondPlayerResult = validatePlayerResult(secondPlayerPoints);
            score = firstPlayerResult + "-" + secondPlayerResult;
        }
        if (isOnePlayerWinsAndHaveLessThanFourPoints(secondPlayerPoints, firstPlayerPoints)) {
            secondPlayerResult = validatePlayerResult(secondPlayerPoints);
            firstPlayerResult = validatePlayerResult(firstPlayerPoints);
            score = firstPlayerResult + "-" + secondPlayerResult;
        }

        if (isPlayerHaveAdv(firstPlayerPoints, secondPlayerPoints)) {
            score = "Advantage player1";
        }

        if (isPlayerHaveAdv(secondPlayerPoints, firstPlayerPoints)) {
            score = "Advantage player2";
        }

        if (isPlayerWin(firstPlayerPoints, secondPlayerPoints)) {
            score = "Win for player1";
        }
        if (isPlayerWin(secondPlayerPoints, firstPlayerPoints)) {
            score = "Win for player2";
        }
        return score;
    }

    private boolean isOnePlayerWinsAndHaveLessThanFourPoints(int playerWhoWins, int otherPlayer) {
        return playerWhoWins > otherPlayer && playerWhoWins < 4;
    }

    private boolean isOnePlayerIsHaveZeroPoints(int otherPlayer, int playerWithZeroPoints) {
        return otherPlayer > 0 && playerWithZeroPoints == 0;
    }

    private boolean isScoreDeuce() {
        return firstPlayerPoints == secondPlayerPoints && firstPlayerPoints >= 3;
    }

    private boolean isScoreIsTheSame() {
        return firstPlayerPoints == secondPlayerPoints && firstPlayerPoints < 4;
    }

    private boolean isPlayerWin(int playerWhoWin, int otherPlayer) {
        int differance = playerWhoWin - otherPlayer;
        return playerWhoWin >= 4 && otherPlayer >= 0 && differance >= 2;
    }

    private boolean isPlayerHaveAdv(int playerWithAdv, int otherPlayer) {
        return playerWithAdv > otherPlayer && otherPlayer >= 3;
    }

    private String validatePlayerResult(int playerPoints) {
        String tempResults = "";
        switch (playerPoints) {
            case 0:
                tempResults = "Love";
                break;
            case 1:
                tempResults = "Fifteen";
                break;
            case 2:
                tempResults = "Thirty";
                break;
            case 3:
                tempResults = "Forty";
        }
        return tempResults;
    }

    public void setFirstPlayerScore(int number) {

        for (int i = 0; i < number; i++) {
            addPointToFirstPlayer();
        }

    }

    public void setSecondPlayerScore(int number) {

        for (int i = 0; i < number; i++) {
            addPointToSecondPlayer();
        }

    }

    public void addPointToFirstPlayer() {
        firstPlayerPoints++;
    }

    public void addPointToSecondPlayer() {
        secondPlayerPoints++;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals("player1")) {
            addPointToFirstPlayer();
        } else {
            addPointToSecondPlayer();
        }
    }
}