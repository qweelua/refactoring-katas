
public class TennisGame3 implements TennisGame {

    private int secondPlayerPoints;
    private int firstPlayerPoints;
    private final String firstPlayerName;
    private final String secondPlayerName;

    public TennisGame3(String firstPlayerName, String secondPlayerName) {
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
    }

    public String getScore() {
        String score;
        if (isPlayersScoresLessThenFour() && isSumScoreLessThenSix()) {
            String[] pointsNames = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
            score = pointsNames[firstPlayerPoints];
            return (firstPlayerPoints == secondPlayerPoints)
                    ? score + "-All"
                    : score + "-" + pointsNames[secondPlayerPoints];
        } else {
            if (firstPlayerPoints == secondPlayerPoints) {
                return "Deuce";
            }
            score = getHigherScore();
            return isItAdvRound()
                    ? "Advantage " + score
                    : "Win for " + score;
        }
    }

    private String getHigherScore() {
        return firstPlayerPoints > secondPlayerPoints ? firstPlayerName : secondPlayerName;
    }

    private boolean isItAdvRound() {
        return (firstPlayerPoints - secondPlayerPoints) * (firstPlayerPoints - secondPlayerPoints) == 1;
    }

    private boolean isSumScoreLessThenSix() {
        return firstPlayerPoints + secondPlayerPoints != 6;
    }

    private boolean isPlayersScoresLessThenFour() {
        return firstPlayerPoints < 4 && secondPlayerPoints < 4;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals("player1")) {
            this.firstPlayerPoints += 1;
        } else {
            this.secondPlayerPoints += 1;
        }
    }

}
