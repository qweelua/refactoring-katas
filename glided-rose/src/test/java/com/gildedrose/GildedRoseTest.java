package com.gildedrose;

import com.gildedrose.item.Item;
import com.gildedrose.item.strategy.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GildedRoseTest {

    @Test
    void testIfItemIsSulfas() {
        Item[] items = new Item[]{
                new Item("Sulfuras, Hand of Ragnaros", 0, 0, new SulfasStrategy()),
                new Item("Sulfuras, Hand of Ragnaros", -1, 9, new SulfasStrategy()),
                new Item("Sulfuras, Hand of Ragnaros", -2000, 6, new SulfasStrategy())
        };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertAll(
                () -> assertEquals(80, items[0].quality),
                () -> assertEquals(80, items[1].quality),
                () -> assertEquals(80, items[2].quality)
        );
    }

    @Test
    void testIfItemIsAgedBrie() {
        Item[] items = new Item[]{
                new Item("Aged Brie", 0, 1, new BrieStrategy()),
                new Item("Aged Brie", 10, 9, new BrieStrategy()),
                new Item("Aged Brie", -1, 2, new BrieStrategy())
        };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertAll(
                () -> assertEquals(3, items[0].quality),
                () -> assertEquals(10, items[1].quality),
                () -> assertEquals(4, items[2].quality)
        );
    }

    @Test
    @DisplayName("If item quality 60, update must set 50")
    void testIfItemIsCommon() {
        Item[] items = new Item[]{
                new Item("Common", 0, 1, new DefaultStrategy()),
                new Item("Apple", 10, 60, new DefaultStrategy()),
                new Item("Juice", -1, 2, new DefaultStrategy())
        };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertAll(
                () -> assertEquals(0, items[0].quality),
                () -> assertEquals(50, items[1].quality),
                () -> assertEquals(0, items[2].quality)
        );
    }

    @Test
    void testIfItemIsBackstagePass() {
        Item[] items = new Item[]{
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 1, new BackstageStrategy()),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 60, new BackstageStrategy()),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 2, new BackstageStrategy())
        };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertAll(
                () -> assertEquals(4, items[0].quality),
                () -> assertEquals(50, items[1].quality),
                () -> assertEquals(4, items[2].quality)
        );
    }

    @Test
    void testIfItemIsConjured() {
        Item[] items = new Item[]{
                new Item("Conjured Mana Cake", 5, 1, new ConjuredStrategy()),
                new Item("Conjured Mana Cake", 10, 60, new ConjuredStrategy()),
                new Item("Conjured Mana Cake", 10, 2, new ConjuredStrategy())
        };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertAll(
                () -> assertEquals(-1, items[0].quality),
                () -> assertEquals(48, items[1].quality),
                () -> assertEquals(0, items[2].quality)
        );
    }

}
