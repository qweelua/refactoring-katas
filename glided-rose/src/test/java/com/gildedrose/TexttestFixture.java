package com.gildedrose;

import com.gildedrose.item.Item;
import com.gildedrose.item.strategy.*;

public class TexttestFixture {
    public static void main(String[] args) {
        System.out.println("OMGHAI!");

        Item[] items = new Item[]{
                new Item("+5 Dexterity Vest", 0, 20, new DefaultStrategy()),
                new Item("Aged Brie", 0, 5, new BrieStrategy()), //
                new Item("Elixir of the Mongoose", 5, 7, new DefaultStrategy()), //
                new Item("Sulfuras, Hand of Ragnaros", 0, 80, new SulfasStrategy()), //
                new Item("Sulfuras, Hand of Ragnaros", -1, 80, new SulfasStrategy()),
                new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20, new BackstageStrategy()),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 47, new BackstageStrategy()),
                new Item("Backstage passes to a TAFKAL80ETC concert", 0, 47, new BackstageStrategy()),
                // this conjured item does not work properly yet
                new Item("Conjured Mana Cake", -1, 6, new ConjuredStrategy())};

        GildedRose app = new GildedRose(items);

        int days = 2;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            for (Item item : items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }
    }

}
