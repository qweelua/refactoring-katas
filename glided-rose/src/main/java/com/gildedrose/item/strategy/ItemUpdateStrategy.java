package com.gildedrose.item.strategy;

import com.gildedrose.item.Item;

public interface ItemUpdateStrategy {
    default void updateSellIn(Item item) {
        item.sellIn--;
    }

    default void updateQualityForItem(Item item) {
        if (!isItemQualityLessThenFifty(item)) {
            item.quality = 50;
        } else if (item.quality > 0) {
            decrementQuality(item);
        }
    }

    void updateQualityIfSellInIsOver(Item item);

    default void decrementQuality(Item item) {
        item.quality--;
    }

    default void upQuality(Item item) {
        item.quality++;
    }

    default boolean isItemQualityLessThenFifty(Item item) {
        return item.quality < 50;
    }

}
