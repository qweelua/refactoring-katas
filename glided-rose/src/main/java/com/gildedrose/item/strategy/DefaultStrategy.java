package com.gildedrose.item.strategy;

import com.gildedrose.item.Item;

public class DefaultStrategy implements ItemUpdateStrategy {
    @Override
    public void updateQualityIfSellInIsOver(Item item) {
        if (item.quality > 0 && isItemQualityLessThenFifty(item)) {
            decrementQuality(item);
        }
    }

}
