package com.gildedrose.item.strategy;

import com.gildedrose.item.Item;

public class ConjuredStrategy implements ItemUpdateStrategy {
    @Override
    public void updateQualityForItem(Item item) {
        if (!isItemQualityLessThenFifty(item)) {
            item.quality = 50;
        }
        if (item.quality > 0) {
            decrementQuality(item);
        }
    }

    @Override
    public void updateQualityIfSellInIsOver(Item item) {
        decrementQuality(item);
    }

}
