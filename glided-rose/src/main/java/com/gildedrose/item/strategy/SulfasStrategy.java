package com.gildedrose.item.strategy;

import com.gildedrose.item.Item;

public class SulfasStrategy implements ItemUpdateStrategy {
    @Override
    public void updateQualityForItem(Item item) {
        item.quality = 80;
    }

    @Override
    public void updateQualityIfSellInIsOver(Item item) {
        item.quality = 80;
    }

    @Override
    public void updateSellIn(Item item) {
        // sulfas object don't change sell in
    }

}
