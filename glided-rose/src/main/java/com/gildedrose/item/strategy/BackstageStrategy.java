package com.gildedrose.item.strategy;

import com.gildedrose.item.Item;

public class BackstageStrategy implements ItemUpdateStrategy {
    @Override
    public void updateQualityForItem(Item item) {
        if (isItemQualityLessThenFifty(item)) {
            upQuality(item);
            if (item.sellIn < 11 && isItemQualityLessThenFifty(item)) {
                upQuality(item);
            }
            if (item.sellIn < 6 && isItemQualityLessThenFifty(item)) {
                upQuality(item);
            }
        } else {
            item.quality = 50;
        }
    }

    @Override
    public void updateQualityIfSellInIsOver(Item item) {
        if (item.sellIn < 0) {
            item.quality = 0;
        }
    }

}
