package com.gildedrose.item;

import com.gildedrose.item.strategy.ItemUpdateStrategy;

public class Item {

    public String name;
    public ItemUpdateStrategy strategy;
    public int sellIn;
    public int quality;

    public Item(String name, int sellIn, int quality, ItemUpdateStrategy strategy) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
        this.strategy = strategy;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }
}
