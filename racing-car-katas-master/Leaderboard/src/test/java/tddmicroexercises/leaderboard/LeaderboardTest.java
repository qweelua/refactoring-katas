package tddmicroexercises.leaderboard;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static tddmicroexercises.leaderboard.TestData.*;

public class LeaderboardTest {

    @Test
    public void itShouldSumThePoints() {
        // setup

        // act
        Map<String, Integer> results = TestData.sampleLeaderboard1.driverResults();

        // verify
        assertAll(
                () -> assertTrue(results.containsKey("Lewis Hamilton"), "results " + results),
                () -> assertEquals(18 + 18 + 25, (int) results.get("Lewis Hamilton"))

        );
    }

    @Test
    public void isShouldFindTheWinner() {
        // setup

        // act
        List<String> result = TestData.sampleLeaderboard1.driverRankings();

        // verify
        assertEquals("Lewis Hamilton", result.get(0));
    }

    @Test
    public void itShouldKeepAllDriversWhenSamePoints() {
        // setup
        // bug, drops drivers with same points
        Race winDriver1 = new Race("Australian Grand Prix", driver1, driver2, driver3);
        Race winDriver2 = new Race("Malaysian Grand Prix", driver2, driver1, driver3);
        Leaderboard exEquoLeaderBoard = new Leaderboard(winDriver1, winDriver2);

        // act
        List<String> rankings = exEquoLeaderBoard.driverRankings();

        // verify
        assertEquals(Arrays.asList(driver3.getName(), driver1.getName(), driver2.getName()), rankings);
        // note: the order of driver1 and driver2 is JDK/platform dependent
    }

    @Test
    void testRaceWithSelfDrivenCar() {
        Race cherkassy_grand_prix = new Race("Cherkassy Grand Prix", driver4, driver3, driver4);
        Race kaniv_grand_prix = new Race("Kaniv Grand Prix", driver4, driver3, driver1);
        Leaderboard leaderboard = new Leaderboard(cherkassy_grand_prix,kaniv_grand_prix);

        List<String> results = leaderboard.driverRankings();

        assertEquals("Self Driving Car - " + driver4.getRepresentatives() + " (" + driver4.getName() + ")", results.get(0));
    }


}
