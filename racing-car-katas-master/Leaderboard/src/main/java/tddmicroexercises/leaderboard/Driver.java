package tddmicroexercises.leaderboard;

import java.util.Objects;

public class Driver implements RaceParticipants{

	private final String name;
	private final String country;

	public Driver(String name, String country) {
		this.name = name;
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public String getRepresentatives() {
		return country;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Driver driver = (Driver) o;
		return Objects.equals(getName(), driver.getName()) &&
				Objects.equals(getRepresentatives(), driver.getRepresentatives());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName(), getRepresentatives());
	}
}
