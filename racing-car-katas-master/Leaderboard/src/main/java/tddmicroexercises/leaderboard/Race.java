package tddmicroexercises.leaderboard;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Race {

	private static final Integer[] POINTS = new Integer[] { 25, 18, 15 };

	private final String name;
	private final List<RaceParticipants> results;
	private final Map<RaceParticipants, String> driverNames;

	public Race(String name, RaceParticipants ... drivers) {
		this.name = name;
		this.results = Arrays.asList(drivers);
		this.driverNames = new HashMap<>();
		for (RaceParticipants driver : results) {
			String driverName = driver.getName();
			if (driver instanceof SelfDrivingCar) {
				driverName = "Self Driving Car - " + driver.getRepresentatives() + " (" + ((SelfDrivingCar) driver)
						.getAlgorithmVersion() + ")";
			}
			this.driverNames.put(driver, driverName);
		}
	}

	public int position(RaceParticipants driver) {
		return this.results.indexOf(driver);
	}

	public int getPoints(RaceParticipants driver) {
		return Race.POINTS[position(driver)];
	}

	public List<RaceParticipants> getResults() {
		return results;
	}

	public String getDriverName(RaceParticipants driver) {
		return this.driverNames.get(driver);
	}

	@Override
	public String toString() {
		return name;
	}
}
