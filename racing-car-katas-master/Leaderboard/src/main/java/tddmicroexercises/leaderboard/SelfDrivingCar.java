package tddmicroexercises.leaderboard;

public class SelfDrivingCar implements RaceParticipants {

	private String algorithmVersion;
	private final String company;

	public SelfDrivingCar(String algorithmVersion, String company) {
		this.company = company;
		this.algorithmVersion = algorithmVersion;
	}

	public String getAlgorithmVersion() {
		return algorithmVersion;
	}

	public void setAlgorithmVersion(String algorithmVersion) {
		this.algorithmVersion = algorithmVersion;
	}

	@Override
	public String getName() {
		return algorithmVersion;
	}

	@Override
	public String getRepresentatives() {
		return company;
	}
}
