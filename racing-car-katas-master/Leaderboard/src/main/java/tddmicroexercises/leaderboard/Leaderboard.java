package tddmicroexercises.leaderboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Leaderboard {

	private final List<Race> races;

	public Leaderboard(Race... races) {
		this.races = Arrays.asList(races);
	}

	public Map<String, Integer> driverResults() {
		Map<String, Integer> results = new HashMap<>();
		for (Race race : this.races) {
			for (RaceParticipants driver : race.getResults()) {
				String driverName = race.getDriverName(driver);
				int points = race.getPoints(driver);
				if (results.containsKey(driverName)) {
					results.put(driverName, results.get(driverName) + points);
				} else {
					results.put(driverName, points);
				}
			}
		}
		return results;
	}

	public List<String> driverRankings() {
		Map<String, Integer> results = driverResults();
		List<String> resultsList = new ArrayList<>(results.keySet());
		resultsList.sort(new DriverByPointsDescendingComparator(results).reversed());
		return resultsList;
	}

	private static final class DriverByPointsDescendingComparator implements Comparator<String> {
		private final Map<String, Integer> results;

		private DriverByPointsDescendingComparator(Map<String, Integer> results) {
			this.results = results;
		}

		@Override
		public int compare(String driverName1, String driverName2) {
			return results.get(driverName1).compareTo(results.get(driverName2));
		}
	}

}
