package tddmicroexercises.leaderboard;

public interface RaceParticipants {
    String getName();
    String getRepresentatives();
}
