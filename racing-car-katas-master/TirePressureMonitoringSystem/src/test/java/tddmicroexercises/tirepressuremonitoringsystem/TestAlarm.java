package tddmicroexercises.tirepressuremonitoringsystem;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestAlarm {

	@Test
	void testIfOnceReturnTrueAlarmAlwaysMustBeTrue() {
		Sensor sensor = new Sensor();
		Alarm alarm = new Alarm(sensor);
		alarm.check();
		alarm.check();
		alarm.check();
		alarm.check();
		alarm.check();
		assertTrue(alarm.isAlarmOn());

	}
}
