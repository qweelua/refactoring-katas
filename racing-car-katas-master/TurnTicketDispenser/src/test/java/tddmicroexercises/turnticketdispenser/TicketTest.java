package tddmicroexercises.turnticketdispenser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TicketTest {

    @Test
    void testRealization() {
        TicketDispenser ticketDispenser = new TicketDispenser();
        ticketDispenser.getTurnTicket();
        TicketDispenser ticketDispenser1 = new TicketDispenser();
        ticketDispenser1.getTurnTicket();
        TicketDispenser ticketDispenser2 = new TicketDispenser();
        ticketDispenser2.getTurnTicket();
        ticketDispenser2.getTurnTicket();
        assertEquals(4, ticketDispenser2.getTurnTicket().getTurnNumber());
    }
}