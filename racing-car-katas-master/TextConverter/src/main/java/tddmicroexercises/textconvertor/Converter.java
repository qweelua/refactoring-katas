package tddmicroexercises.textconvertor;

public interface Converter {
    String getFilename();
}
