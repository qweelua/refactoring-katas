package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HtmlTextConverterTest {

    HtmlTextConverter converter;

    @BeforeEach
    void innit() {
        converter = new HtmlTextConverter("src/test/java/tddmicroexercises/textconvertor/test.txt");

    }

    @Test
    void testConvertToHtml() throws IOException {
        String expected = "&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;<br />";
        String convertToHtml = converter.convertToHtml();
        assertEquals(expected, convertToHtml);
    }

    @Test
    void getNameTest() {
        assertEquals("src/test/java/tddmicroexercises/textconvertor/test.txt", converter.getFilename());
    }

}
