package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HtmlPagesConverterTest {

    HtmlPagesConverter converter;

    @BeforeEach
    void innit() {
        converter = new HtmlPagesConverter("src/test/java/tddmicroexercises/textconvertor/test.txt");

    }

    @Test
    void testGetName() {
        assertEquals("src/test/java/tddmicroexercises/textconvertor/test.txt", converter.getFilename());
    }

    @Test
    void testConvert() throws IOException {
        String htmlPage = converter.getHtmlPage(0);
        String expected = "&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;<br />";
        assertEquals(expected, htmlPage);
    }

}
