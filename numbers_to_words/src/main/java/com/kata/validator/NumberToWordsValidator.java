package com.kata.validator;

public class NumberToWordsValidator {
    public String validateForOneDigitNumber(int num) {
        switch (num) {
            case 1:
                return "one";
            case 2:
                return "two";
            case 3:
                return "three";
            case 4:
                return "four";
            case 5:
                return "five";
            case 6:
                return "six";
            case 7:
                return "seven";
            case 8:
                return "eight";
            case 9:
                return "nine";
            case 0:
                return "zero";
            default:
                return "wrong num";
        }
    }

    public String validateForTwoDigitNumber(int num) {
        String digitsDescription = "";
        if (num < 21 && num > 9) {
            return getDescriptionFromTenToTwenty(num);
        }
        if (num > 20 && num < 100) {
            digitsDescription = getTensDescription(num) + validateForOneDigitNumber(num % 10);
        }
        return digitsDescription;
    }

    private String getTensDescription(int num) {
        String digitsDescription;
        int tempNum = num / 10;
        switch (tempNum) {
            case 2:
                digitsDescription = "twenty-";
                break;
            case 3:
                digitsDescription = "thirty-";
                break;
            case 5:
                digitsDescription = "fifty-";
                break;
            case 8:
                digitsDescription = "eighty-";
                break;
            default:
                digitsDescription = validateForOneDigitNumber(tempNum) + "ty-";
        }
        return digitsDescription;
    }

    private String getDescriptionFromTenToTwenty(int num) {
        switch (num) {
            case 10:
                return "ten";
            case 11:
                return "eleven";
            case 12:
                return "twelve";
            case 13:
                return "thirteen";
            case 15:
                return "fifteen";
            case 18:
                return "eighteen";
            case 20:
                return "twenty";
            default:
                return validateForOneDigitNumber(num % 10) + "teen";
        }
    }

    public String validateForThreeDigitNumber(int num) {
        String digitsDescription = "";
        digitsDescription = validateForOneDigitNumber(num / 100) + " hundred";
        if (isSecondDigitIsZero(num) && num % 10 == 0) {
            return digitsDescription;
        }
        if (isSecondDigitIsZero(num)) {
            digitsDescription = digitsDescription + " " + validateForOneDigitNumber(num % 10);
        } else {
            digitsDescription = digitsDescription + " " + validateForTwoDigitNumber(num % 100);
        }
        return digitsDescription;
    }

    public String validateForFourDigitNumber(int num) {
        String digitDescription = "";
        digitDescription = validateForOneDigitNumber(num / 1000) + " thousand";
        if (num % 1000 == 0) {
            return digitDescription;
        } else {
            digitDescription = digitDescription + " " + validateForThreeDigitNumber(num % 1000);
        }
        return digitDescription;
    }

    private boolean isSecondDigitIsZero(int num) {
        return (num / 10) % 10 == 0;
    }
}
