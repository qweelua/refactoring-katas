package com.kata.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberToWordsValidatorTest {
    NumberToWordsValidator numberToWordsValidator;

    @BeforeEach
    void setUp() {
        numberToWordsValidator = new NumberToWordsValidator();
    }

    @Test
    @DisplayName("When digit is one")
    void testValidateForOneDigit() {
        assertAll(
                () -> assertEquals("one", numberToWordsValidator.validateForOneDigitNumber(1)),
                () -> assertEquals("zero", numberToWordsValidator.validateForOneDigitNumber(0)),
                () -> assertEquals("nine", numberToWordsValidator.validateForOneDigitNumber(9))
        );
    }

    @Test
    @DisplayName("When digits is two")
    void testValidateForTwoDigit() {
        assertAll(
                () -> assertEquals("twenty-two", numberToWordsValidator.validateForTwoDigitNumber(22)),
                () -> assertEquals("twelve", numberToWordsValidator.validateForTwoDigitNumber(12)),
                () -> assertEquals("ninety-five", numberToWordsValidator.validateForTwoDigitNumber(95)),
                () -> assertEquals("sixteen", numberToWordsValidator.validateForTwoDigitNumber(16))
        );
    }

    @Test
    @DisplayName("When digits is three")
    void testValidateForThreeDigits() {
        assertAll(
                () -> assertEquals("one hundred", numberToWordsValidator.validateForThreeDigitNumber(100)),
                () -> assertEquals("three hundred three", numberToWordsValidator.validateForThreeDigitNumber(303)),
                () -> assertEquals("five hundred fifty-five", numberToWordsValidator.validateForThreeDigitNumber(555))
        );
    }

    @Test
    @DisplayName("When digits is four")
    void testValidateForFourDigits() {
        assertAll(
                () -> assertEquals("two thousand",
                        numberToWordsValidator.validateForFourDigitNumber(2000)),
                () -> assertEquals("three thousand four hundred sixty-six",
                        numberToWordsValidator.validateForFourDigitNumber(3466)),
                () -> assertEquals("two thousand four hundred",
                        numberToWordsValidator.validateForFourDigitNumber(2400))
        );
    }

}